﻿<a class="brand" href="http://www.arrkgroup.com/">
<img src="http://cl.ly/image/1m1A2w040x29/arrklogo.png" alt="Arrk Group">
</a>

Arrk Group http://www.arrkgroup.com

# Introduction

Arrk is committed to evaluating and investing in future technology. As part of that commitment we perform proof-of-concept evaluations. Here we present such a proof-of-concept: a simple Book Search/Storage application using Neo4j.

# BookSearch

Before generating your application, you will need:

* The Ruby language - version 2.2.2 or above
* The Rails gem - version 5.0.1

See the article "Installing Rails":http://railsapps.github.io/installing-rails.html for instructions about setting up Rails and your development environment.

Getting the Application

Local

You have several options for getting the code on your own machine. You can _fork_, _clone_, or _generate_.

Fork

If you'd like to add features (or bug fixes) to improve the example application, you can fork the GitHub repo and make pull requests. Your code contributions are welcome!

Clone

If you want to copy and customize the app with changes that are only useful for your own project, you can clone the GitHub repo. You'll need to search-and-replace the project name throughout the application. You probably should generate the app instead (see below). To clone:

<pre>
$ git clone git@gitlab.com:arrk-open-source/neo4jrb_books_portal.git
</pre>

You'll need "git":http://git-scm.com/ on your machine. See "Rails and Git":http://railsapps.github.io/rails-git.html.

h3. Install the Required Gems
you should run the @bundle install@ command to install the required gems on your computer:

<pre>
$ bundle install
</pre>

You can check which gems are installed on your computer with:

<pre>
$ gem list
</pre>

Keep in mind that you have installed these gems locally. When you deploy the app to another server, the same gems (and versions) must be available.

CentOS/RedHat users. Please install java and nodejs by below commands. Other users please check for the installation of java and nodejs :
<pre>
  $ sudo yum install java-1.8.0-openjdk.x86_64
  $ sudo yum install nodejs
</pre>

Install Neo4j:
<pre>
  $ rake neo4j:install[community-latest]
</pre>

Start Neo4j:
<pre>
  $ rake neo4j:start  
  $ rake neo4j:generate_schema_migration[constraint,Asset,uuid]  
  $ rake neo4j:generate_schema_migration[constraint,Category,uuid]  
  $ rake neo4j:generate_schema_migration[constraint,User,uuid]  
  $ rake neo4j:migrate
</pre>
Visit the Neo4j web console if you want to browse the data with Cypher http://localhost:7474

Start Rails:
<pre>
$ rails s
</pre>

To browse the application http://localhost:3000
