class Book < Asset
  include Neo4j::ActiveNode
  property :isbn, type: String
  property :title, type: String
  property :year_published, type: Integer

  has_one :in, :author, type: :CREATED, model_class: :User
  has_many :out, :categories, type: :HAS_CATEGORY

  searchkick index_name: "books_v1"
  def search_data
   {
    id: uuid,
    title: title,
    author: self.author.name,
    email: self.author.email,
    categories: self.categories.collect(&:name),
    year_published: year_published,
    isbn: isbn
    }
  end

end
